<?php
defined('ABSPATH') or die("No script kiddies please!");
//require_once('inc/bs-walker.php');
define('THEMEDIR', get_template_directory_uri());
define('STYLES', THEMEDIR . '/assets/styles/');
define('SCRIPTS', THEMEDIR . '/assets/scripts/');
define('IMG', THEMEDIR . '/assets/img/');
define('PLUGINS', THEMEDIR . '/assets/plugins/');
define('NODE', THEMEDIR . '/node_modules/');

define('TEXTDOMAIN', 'afek');


// ******* SETUP *******
load_theme_textdomain(TEXTDOMAIN, get_template_directory() . '/languages');

add_filter('show_admin_bar', '__return_false');

function disable_wp_emojicons($plugins)
{
    if (is_array($plugins)) {
        return array_diff($plugins, array('wpemoji'));
    } else {
        return array();
    }
}

add_action('init', 'disable_wp_emojicons');

//Images sizes
add_image_size('gallery-thumb', 134, 94);


function scriptsInit()
{

    wp_enqueue_style('bootstrap', STYLES . 'bootstrap_app.css', false, 1.0);
    wp_enqueue_script('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array('jquery'), 1.0, false);
    wp_enqueue_script('bootstrap', NODE . 'bootstrap/dist/js/bootstrap.min.js', array('jquery'), 1.0, false);


    wp_enqueue_style('slick-css', PLUGINS . 'slick/slick.css', false, 1.0);
    wp_enqueue_style('slick-theme-css', PLUGINS . 'slick/slick-theme.css', false, 1.0);
    wp_enqueue_script('slick-js', PLUGINS . 'slick/slick.min.js', array('jquery'), 1.0, true);

    wp_enqueue_style('lightbox-css', PLUGINS . 'lightbox/src/css/lightbox.css', false, 1.0);
    wp_enqueue_script('lightbox-js', PLUGINS . 'lightbox/src/js/lightbox.js', array('jquery'), 1.0, true);

    wp_enqueue_style('app-style', STYLES . 'styles.css', false, 1.0);
    $object = array(
        'wp_ajax' => admin_url( 'admin-ajax.php' ),
    );
    wp_enqueue_script('scroll-scripts', PLUGINS . 'scrollbar/jquery.mCustomScrollbar.concat.min.js', array('jquery'), 1.0, true);
    wp_enqueue_script('app-scripts');
    wp_localize_script('app-scripts', 'JSObject', $object);
    wp_enqueue_script('app-scripts', SCRIPTS . 'scripts.js', array('jquery'), 1.0, true);

    wp_enqueue_style('primary-style', get_stylesheet_uri(), '', '1.0');

}

add_action('wp_enqueue_scripts', 'scriptsInit');

function admin_style()
{
    wp_enqueue_style('admin-styles', STYLES . 'admin.css');
}

add_action('admin_enqueue_scripts', 'admin_style');

function registerMenus()
{
    register_nav_menus(
        array(
            'footer-menu' => __('Footer Menu', TEXTDOMAIN),
            'main-menu' => __('Main Menu', TEXTDOMAIN),
            'sidebar-menu' => __('Sidebar Menu', TEXTDOMAIN),
        )
    );
}

add_action('init', 'registerMenus');

if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'הגדרות נוספות',
        'menu_title' => 'הגדרות נוספות',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));
}

add_theme_support( 'post-thumbnails' );

add_filter('acf/settings/save_json', 'acf_json_save_point');
//acf json init
function acf_json_save_point( $path ) {
    $path = get_stylesheet_directory() . '/acf';
    return $path;
}


add_filter('acf/settings/load_json', 'acf_json_load_point');
function acf_json_load_point( $paths ) {
    unset($paths[0]);
    $paths[] = get_stylesheet_directory() . '/acf';
    return $paths;
}

require_once "inc/ajax_calls.php";
require_once "inc/helpers.php";