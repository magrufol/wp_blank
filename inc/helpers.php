<?php


function text_preview($phrase, $max_words) {
    $phrase = strip_shortcodes($phrase);
    $phrase = strip_tags($phrase);
    $phrase_array = explode(' ',$phrase);
    if(count($phrase_array) > $max_words && $max_words > 0)
        $phrase = implode(' ',array_slice($phrase_array, 0, $max_words)).'...';
    return $phrase;
}

function postThumb($post = false, $size = 'full', $placeholder_size = 'regular', $placeholder = true){
    $postThumb = IMG . 'placeholder-'. $placeholder_size .'.png';
    if(!$post){
        $post = get_post();
    }
    if($post && get_the_post_thumbnail_url($post)){
        $postThumb = get_the_post_thumbnail_url($post, $size);
    }

    if($placeholder){
        return $postThumb;
    }else{
        return false;
    }

}
function getYoutubeId($url){
    parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
    return $my_array_of_vars['v'];
}
function getYoutubeTumb($url){
    return "https://img.youtube.com/vi/<?= getYoutubeId(".getYoutubeId($url).") ?>/0.jpg";
}
function getForm($id){
    echo do_shortcode('[contact-form-7 id="'.$id.'"  html_class="use-floating-validation-tip"]');
}
function svg($path){
    return file_get_contents($path);
}
function getMenu($location, $depth, $menu_class = ''){
    wp_nav_menu([
        'theme_location' => $location,
        'container'       => 'div',
        'container_id'    => '',
        'container_class' => '',
        'menu_id'         => false,
        'menu_class'      => $menu_class,
        'depth'           => $depth,
    ]);
}
function opt($field_name){
    return get_field($field_name, 'options');
}